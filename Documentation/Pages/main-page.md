### TNL::SPH - Smoothed Particle Hydrodynamics library

TNL::SPH is a library designed to perform SPH simulations and build SPH solver. It provides ready-to use SPH solvers, which are able to solve industrial applications as well as tools allowing easy and fast developement of own SPH solvers and simulations.
