# How to setup and run benchmarks

Setting up and running benchmarks is still somewhat difficult as it involves comparison with several other solvers. Since each solver (library) is handled completely differently, it is quite difficult to make a fully automated benchmark script.

Currently, some things need to be set up manually and hopefully soon the whole benchmark can be automated using cmake.
